package main

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

type IPInt uint32
type SubnetInt uint32

func usage() {
	fmt.Println("Usage: ipcheck IP-address")
}

func IPIntToStr(IP IPInt) string {
	return fmt.Sprintf("%d.%d.%d.%d",
		(IP>>24)&0xFF, (IP>>16)&0xFF, (IP>>8)&0xFF, IP&0xFF)
}

func IPStrToInt(IPStr string) (IPInt, error) {
	re := regexp.MustCompile(`(?m)^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$`)
	var IP IPInt

	matches := re.FindStringSubmatch(IPStr)

	if matches == nil {
		return 0, errors.New("Invalid IP address")
	}

	for i := 1; i < 5; i++ {
		n, err := strconv.Atoi(matches[i])

		if err != nil {
			return 0, err
		}

		if n&0xFF != n || i == 1 && n == 0 {
			return 0, errors.New("Invalid IP address")
		} else {
			IP += IPInt((n & 0xFF) << (24 - 8*(i-1)))
		}
	}

	return IP, nil
}

func findIPClass(octet byte) byte {
	switch {
	case octet <= 126:
		return 'A'
	case octet == 127:
		return 'L'
	case octet >= 128 && octet <= 191:
		return 'B'
	case octet >= 192 && octet <= 223:
		return 'C'
	case octet >= 224 && octet <= 239:
		return 'D'
	case octet >= 240 && octet <= 255:
		return 'E'
	default:
		// We shouldn't get here, but...
		return 0
	}
}

func isPrivate(IP IPInt) bool {
	if ((IP >> 24) & 0xFF) == 10 {
		// 10.0.0.0 to 10.255.255.255
		return true
	} else if ((IP>>24)&0xFF) == 172 &&
		((IP>>16)&0xFF) >= 16 && ((IP>>16)&0xFF) <= 31 {
		// 172.16.0.0 to 172.31.255.255
		return true
	} else if ((IP>>24)&0xFF) == 192 &&
		((IP>>16)&0xFF) == 168 {
		// 192.168.0.0 to 192.168.255.255
		return true
	}

	return false
}

func findDefaultSubnet(class byte) (string, SubnetInt) {
	switch class {
	case 'A':
		return "255.0.0.0", 0xFF << 24
	case 'B':
		return "255.255.0.0", 0xFFFF << 16
	case 'C':
		return "255.255.255.0", 0xFFFFFF << 8
	default:
		return "N/A", 0
	}
}

func isValidNetandHost(IP IPInt, subnet SubnetInt) bool {

	// If subnet is zero (i.e. Class D/E or 127.*.*.*.*)
	if subnet == 0 && (IP>>24) == 127 {
		subnet = 0xFF << 24
		// Keep going, assume Class A
	} else if subnet == 0 && (IP>>24) != 127 {
		// Just return that IP is valid
		return true
	}

	// Net portion
	if SubnetInt(IP)&subnet == subnet || SubnetInt(IP) & ^subnet == ^subnet {
		return false
	// Host portion
	} else if SubnetInt(IP)&subnet == 0 || SubnetInt(IP) & ^subnet == 0 {
		return false
	}

	return true
}

// if network portion of IPs match, then return true
func isLocalIP(IP IPInt, secondIP IPInt, subnet SubnetInt) bool {
	if IP&IPInt(subnet) == secondIP&IPInt(subnet) {
		return true
	}

	return false
}

func main() {
	if len(os.Args) == 1 {
		usage()
		os.Exit(1)
	}

	fmt.Println("IP Address:", os.Args[1])

	IP, err := IPStrToInt(os.Args[1])
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	octet := byte(((IP >> 24) & 0xFF))
	class := findIPClass(octet)
	subnetStr, subnet := findDefaultSubnet(class)

	if !isValidNetandHost(IP, subnet) {
		fmt.Println("Error: IP is not valid")
		os.Exit(1)
	}

	fmt.Printf("Class %c", class)
	if isPrivate(IP) {
		fmt.Printf(" - private")
	}
	fmt.Println("\nSubnet Mask:", subnetStr)

	if len(os.Args) == 2 {
		os.Exit(0)
	}

	fmt.Println("\nIP Address: ", os.Args[2])

	secondIP, err := IPStrToInt(os.Args[2])
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	secondOctet := byte(((secondIP >> 24) & 0xFF))
	secondClass := findIPClass(secondOctet)
	secondSubnetStr, secondSubnet := findDefaultSubnet(secondClass)

	if !isValidNetandHost(secondIP, secondSubnet) {
		fmt.Println("Error: IP is not valid")
		os.Exit(1)
	}

	fmt.Printf("Class %c", secondClass)
	if isPrivate(IP) {
		fmt.Printf(" - private")
	}
	fmt.Println("\nSubnet Mask:", secondSubnetStr)

	if isLocalIP(IP, secondIP, subnet) {
		fmt.Println("IP addresses are local")
	} else {
		fmt.Println("IP addresses are remote")
	}

}
