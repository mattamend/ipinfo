# ipinfo
ipinfo is a command-line program that gives information on IP addresses.

NOTE: most information given by ipinfo is not practical in most scenarios

## Installation
The binary for ipinfo is not distributed in this repo, you must compile it yourself.

```bash
git clone https://gitlab.com/mattamend/ipinfo
cd ipinfo
go build ipinfo.go
```

This will compile the program and give you a working binary. If you want to put this in your path directory, feel free to do so.

This is a good example on how to do that:
```bash
cp ipinfo /usr/bin/ipinfo
```

## Usage
TODO

## Contributing
This is mostly a pet project, but contributions are welcome, although they will likely be ignored.

## License
[GNU GPL v3](https://choosealicense.com/licenses/gpl-3.0/)